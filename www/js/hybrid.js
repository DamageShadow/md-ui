/*jshint strict:false */
//Stocks

$(document).ready(function () {

    var animating = false,
        submitPhase1 = 1100,
        submitPhase2 = 400,
        logoutPhase1 = 800,
        $login = $(".login"),
        $app = $(".app");

    function ripple(elem, e) {
        $(".ripple").remove();
        var elTop = elem.offset().top,
            elLeft = elem.offset().left,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var $ripple = $("<div class='ripple'></div>");
        $ripple.css({top: y, left: x});
        elem.append($ripple);
    };

    $(document).on("click", ".login__submit", function (e) {
        if (animating) {
            return;
        }
        animating = true;
        var that = this;
        ripple($(that), e);
        $(that).addClass("processing");
        setTimeout(function () {
            $(that).addClass("success");
            setTimeout(function () {
                $app.show();
                $app.css("top");
                $app.addClass("active");
            }, submitPhase2 - 70);
            setTimeout(function () {
                $login.hide();
                $login.addClass("inactive");
                animating = false;
                $(that).removeClass("success processing");
            }, submitPhase2);
        }, submitPhase1);
    });

    $(document).on("click", ".app__logout", function (e) {
        if (animating) return;
        $(".ripple").remove();
        animating = true;
        var that = this;
        $(that).addClass("clicked");
        setTimeout(function () {
            $app.removeClass("active");
            $login.show();
            $login.css("top");
            $login.removeClass("inactive");
        }, logoutPhase1 - 120);
        setTimeout(function () {
            $app.hide();
            animating = false;
            $(that).removeClass("clicked");
        }, logoutPhase1);
    });

    ReactDOM.render(React.createElement(App, {stocks: stocks, refresh: false}), document.getElementById("application"));

});


//Stocks

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var App = function (_React$Component) {
    _inherits(App, _React$Component);

    function App(props) {
        _classCallCheck(this, App);

        var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

        _this.state = {
            stocks: _this.props.stocks,
            refresh: _this.props.refresh || false,
            stockList: [{
                'symbol': '-',
                'LastTradePriceOnly': '-',
                'Change_PercentChange': ' - --'
            }],
            stockDetail: {
                'symbol': '-',
                'LastTradePriceOnly': '-',
                'Change_PercentChange': '- -'
            }
        };
        return _this;
    }

    App.prototype.componentWillMount = function componentWillMount() {
        this.refreshData();
    };

    // componentDidMount() {
    //   var context=this;
    //   if(this.state.refresh){
    //     setTimeout(function(){
    //       context.refreshData()}
    //                ,10000);
    //   }
    // }

    App.prototype.refreshData = function refreshData() {
        var stockList = [];
        var context = this;
        var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.quotes%20where%20symbol%20in%20%28%22' + this.state.stocks.toString() + '%22%29&env=store://datatables.org/alltableswithkeys&format=json';

        $.get(url, function (response) {
            stockList = response.query.results.quote;
            context.setState({
                stockList: stockList,
                stockDetail: stockList[0]
            });
        });
    };

    App.prototype.handleStockEntryClick = function handleStockEntryClick(stock, el) {
        $('.selected').removeClass('selected');
        if (!$(el).hasClass('stock-entry')) {
            el = $(el).parent();
        }
        $(el).addClass('selected');
        this.setState({
            stockDetail: stock
        });
    };

    App.prototype.render = function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                {className: 'stocklist'},
                React.createElement(StockList, {
                    handleStockEntryClick: this.handleStockEntryClick.bind(this),
                    stockList: this.state.stockList
                })
            ),
            React.createElement(
                'div',
                {className: 'stockdetails'},
                React.createElement(StockDetails, {stockDetail: this.state.stockDetail})
            )
        );
    };

    return App;
}(React.Component);

var StockList = function StockList(_ref) {
    var stockList = _ref.stockList;
    var handleStockEntryClick = _ref.handleStockEntryClick;
    return React.createElement(
        'div',
        {className: 'stockListContainer'},
        stockList.map(function (stock) {
            return React.createElement(StockListEntry, {stock: stock, handleStockEntryClick: handleStockEntryClick});
        })
    );
};

var StockListEntry = function StockListEntry(_ref2) {
    var stock = _ref2.stock;
    var handleStockEntryClick = _ref2.handleStockEntryClick;
    return React.createElement(
        'div',
        {
            className: 'stock-entry', onClick: function onClick(el) {
            return handleStockEntryClick(stock, el.target);
        }
        },
        React.createElement(
            'div',
            {className: 'stock'},
            _displaySymbol(stock.symbol, stock.Name)
        ),
        React.createElement(
            'div',
            {className: stock.Change_PercentChange.split(" ")[2].charAt(0) === "+" ? "positive" : "negative"},
            parseFloat(stock.Change_PercentChange.split(" ")[2]).toFixed(2)
        ),
        React.createElement(
            'div',
            {className: 'price'},
            parseFloat(stock.LastTradePriceOnly).toFixed(2)
        )
    );
};

var StockDetails = function StockDetails(_ref3) {
    var stockDetail = _ref3.stockDetail;
    return React.createElement(
        'div',
        {className: 'stockdetails'},
        React.createElement(
            'div',
            {className: 'stockTitle'},
            stockDetail.Name
        ),
        React.createElement(
            'table',
            {className: 'stockTable'},
            React.createElement(
                'tr',
                null,
                React.createElement(
                    'td',
                    null,
                    'OPEN'
                ),
                React.createElement(
                    'td',
                    null,
                    stockDetail.Open
                ),
                React.createElement(
                    'td',
                    null,
                    'MKT CAP'
                ),
                React.createElement(
                    'td',
                    null,
                    _display(stockDetail.MarketCapitalization)
                )
            ),
            React.createElement(
                'tr',
                null,
                React.createElement(
                    'td',
                    null,
                    'HIGH'
                ),
                React.createElement(
                    'td',
                    null,
                    stockDetail.DaysHigh
                ),
                React.createElement(
                    'td',
                    null,
                    '52W HIGH'
                ),
                React.createElement(
                    'td',
                    null,
                    stockDetail.YearHigh
                )
            ),
            React.createElement(
                'tr',
                null,
                React.createElement(
                    'td',
                    null,
                    'LOW'
                ),
                React.createElement(
                    'td',
                    null,
                    stockDetail.DaysLow
                ),
                React.createElement(
                    'td',
                    null,
                    '52W LOW'
                ),
                React.createElement(
                    'td',
                    null,
                    stockDetail.YearLow
                )
            ),
            React.createElement(
                'tr',
                null,
                React.createElement(
                    'td',
                    null,
                    'VOL'
                ),
                React.createElement(
                    'td',
                    null,
                    _display(stockDetail.Volume)
                ),
                React.createElement(
                    'td',
                    null,
                    'AVG VOL'
                ),
                React.createElement(
                    'td',
                    null,
                    _display(stockDetail.AverageDailyVolume)
                )
            ),
            React.createElement(
                'tr',
                null,
                React.createElement(
                    'td',
                    null,
                    'P/E'
                ),
                React.createElement(
                    'td',
                    null,
                    _display(stockDetail.PERatio)
                ),
                React.createElement(
                    'td',
                    null,
                    'YEILD'
                ),
                React.createElement(
                    'td',
                    null,
                    _display(stockDetail.DividendYield)
                )
            )
        )
    );
};

var _display = function _display(field) {
    return field && field.length > 1 ? field : '-';
};

var _displaySymbol = function _displaySymbol(symbol, name) {
    return symbol.charAt(0) === '^' ? name : symbol;
};

var stocks = ['^IXIC', 'AAPL', 'GOOG', 'FB', 'NFLX', 'TWTR', 'MSFT', 'SNAP'];
ReactDOM.render(React.createElement(App, {stocks: stocks, refresh: false}), document.getElementById("application"));